#	project/test_basic.py
#	tests for the aaa docreader api 
import os 
from flask import Flask, jsonify
import unittest
import json 
from app import app

class BasicTest(unittest.TestCase): 
	"""this class represents the test cases for aaa docreader"""
	def setUp(self):
	# 	"""define test variables and initialize app"""
		self.app_context = app.app_context()
		self.app_context.push()
	# 	pass 

	def testGetVehicles(self):
		tester = app.test_client(self) 
		response = tester.get('/aaadocreader/vehicles')
		self.assertEqual(response.status_code, 200)

	def testPostPolicy(self):
		"""test that api can search for policy number and return associated vin numbers"""
		tester = app.test_client(self)
		response = tester.post('/aaadocreader/policy', content_type = 'application/json', data = '{"policynumber": "000000000"}')

		self.assertEqual(response.status_code, 200)

	def testPostVin(self): 
		"""test that api can search for vin number and return associated policy number"""
		tester = app.test_client(self)
		response = tester.post('/aaadocreader/vin', content_type = 'application/json', data = '{"vin": "11111111111111"}')
		self.assertEqual(response.status_code, 200)

	def tearDown(self):
		self.app_context.pop()

if __name__ == '__main__':
	unittest.main()
