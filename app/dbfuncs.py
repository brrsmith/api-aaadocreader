#/app/dbfuncs.py

from app import app, config
from flask import jsonify
from sqlalchemy import create_engine

engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])

def vehicleList():

	c = engine.connect()
	query = c.execute("SELECT policynumber, vin FROM mverityreplica WHERE vin != '-'")

	return jsonify({'vehicles' : [dict(zip(tuple(query.keys()), i)) for i in query.cursor]})


def policySearch(policynum):
	c = engine.connect()
	query = c.execute("SELECT odometer, policynumber, expirationdate, odorecid, docid FROM mverityreplica WHERE policynumber == '%s'" % policynum)

	return jsonify({'policynumber' : '%s' % policynum}, {'vehicleInfo' : [dict(zip(tuple(query.keys()), i)) for i in query.cursor]})

def vinSearch(vinnum):
	c = engine.connect()
	query = c.execute("SELECT odometer, policynumber, expirationdate, odorecid, docid FROM mverityreplica WHERE vin == '%s'" % vinnum)

	return jsonify({'vin' : '%s' % vinnum}, {'vehicleInfo' : [dict(zip(tuple(query.keys()), i)) for i in query.cursor]})

