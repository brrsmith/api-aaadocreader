#/app/routes.py

from flask import request, jsonify
from app import app, dbfuncs, config
from functools import wraps 
# from flask_httpauth import HTTPTokenAuth

@app.route('/')
def index():
	headers = request.headers 
	auth = headers.get("x-api-key")

	if auth == app.config['SECRET_KEY']:
		return jsonify({"message": "ok, authorized"}), 200
	else:
		return jsonify({"message": "ERRROR, unauthorized"}), 401

@app.route('/aaadocreader')
def homepage():
	headers = request.headers 
	auth = headers.get("x-api-key")

	if auth == app.config['SECRET_KEY']:
		return 'welcome to AAA Docreader!', 200
	else:
		return jsonify({"message": "ERRROR, unauthorized"}), 401


@app.route('/aaadocreader/vehicles', methods=['GET'])
def getVehicles():
	#	returns a list of vehicles 
	headers = request.headers 
	auth = headers.get("x-api-key")

	if auth == app.config['SECRET_KEY']:
		return dbfuncs.vehicleList(), 200
	else:
		return jsonify({"message": "ERRROR, unauthorized"}), 401

@app.route('/aaadocreader/policy', methods=['POST'])
# @app.route('/aaadocreader/policy/', methods=['POST'])
def postPolicy():
	headers = request.headers 
	auth = headers.get("x-api-key")

	if auth == app.config['SECRET_KEY']:
		policy = {'policynumber' : request.json['policynumber']}
		policynum = policy['policynumber']
		return dbfuncs.policySearch(policynum), 200
	else:
		return jsonify({"message": "ERRROR, unauthorized"}), 401

@app.route('/aaadocreader/vin', methods = ['POST'])
def postVin():

	headers = request.headers 
	auth = headers.get("x-api-key")

	if auth == app.config['SECRET_KEY']:
		vin = {'vin' : request.json['vin']}
		vinnum = vin['vin']
		return dbfuncs.vinSearch(vinnum), 200
	else:
		return jsonify({"message": "ERRROR, unauthorized"}), 401



