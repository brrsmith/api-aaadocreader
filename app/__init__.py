#/app/__init__.py

from flask import Flask 
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPTokenAuth

app = Flask(__name__)
auth = HTTPTokenAuth(scheme='Token')

app.config.from_pyfile('config.py')

db = SQLAlchemy(app)

from app import routes

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')
	